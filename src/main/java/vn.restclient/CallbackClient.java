package vn.restclient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Copyright © 2016 Neo-Lab Co.,Ltd.
 * Created by Hieu➈ on 31/10/2016.
 * Custom callback Retrofit
 */

public abstract class CallbackClient<T> implements Callback<T> {

    public abstract void onSuccess(Response<T> pObject);

    public abstract void onFailure(String pMsgFailure);


    /**
     * Retrofit Rest Api success
     * */
    @Override
    public void onResponse(Call call, Response response) {
        onSuccess(response);
    }

    /**
     * Retrofit Rest Api failure
     * */
    @Override
    public void onFailure(Call call, Throwable t) {
        onFailure("request fail");
    }
}
